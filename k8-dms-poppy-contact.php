<?php
/*
	Plugin Name: K8 PopUp Contact Form
	Plugin URI: http://www.kri8it.com
	Description: Add a call to action with a contact popup form
	Author: Charl Pretorius
	PageLines: true
	Version: 1.0.2
	Section: true
	Class Name: K8PoppyForm
	Filter: component, forms
	Loading: active
	Cloning: true
	 
	K8 Bitbucket Plugin URI: https://bitbucket.org/kri8it/k8-dms-poppy-contact
*/

/**
 * IMPORTANT
 * This tells wordpress to not load the class as DMS will do it later when the main sections API is available.
 * If you want to include PHP earlier like a normal plugin just add it above here.
 */

if( ! class_exists( 'PageLinesSectionFactory' ) )
	return;

class K8PoppyForm extends PageLinesSection {

	var $tabID = 'highlight_meta';
	
	function section_persistent() {

		$this->base_dir	= plugin_dir_path( __FILE__ );
		$this->base_url = plugins_url( '', __FILE__ );
				
		add_action( 'wp_enqueue_scripts', array( $this, 'hooks_with_activation' ) );
		
		add_action( 'wp_ajax_nopriv_k8_ajaxcontact_send_mail', array( $this, 'ajaxcontact_send_mail' ) );
		add_action( 'wp_ajax_k8_ajaxcontact_send_mail', array( $this, 'ajaxcontact_send_mail' ) );	
		
	}
	
	function hooks_with_activation() {
		wp_enqueue_script( 'k8-poppyjs', plugins_url( '/script.js', __FILE__ ), array( 'jquery' ), $this->version, true );
		wp_localize_script( 'k8-poppyjs', 'k8_poppyjs', array( 'ajaxurl' => admin_url( 'admin-ajax.php' ) ) );

		if( ! function_exists( 'pl_detect_ie' ) )
			return;
		
		$ie_ver = pl_detect_ie();
		if( $ie_ver < 10 )
			wp_enqueue_script( 'k8-formalize', plugins_url( '/formalize.min.js', __FILE__ ), array( 'jquery' ), $this->version, true );
			
	}
	
	function section_opts(){
		$opts = array(
			array(
				'type' 			=> 'select',
				'title' 		=> 'Select Format',
				'key'			=> 'k8_poppy_form_icallout_format',
				'label' 		=> __( 'Callout Format', 'pagelines' ),
				'opts'=> array(
					'top'			=> array( 'name' => __( 'Text on top of button', 'pagelines' ) ),
					'inline'	 	=> array( 'name' => __( 'Text/Button Inline', 'pagelines' ) )
				),
			),
			array(
				'type' 			=> 'multi',
				'col'			=> 2,
				'title' 		=> __( 'Callout Text', 'pagelines' ),
				'opts'	=> array(
					array(
						'key'			=> 'k8_poppy_form_icallout_text',
						'version' 		=> 'pro',
						'type' 			=> 'text',
						'label' 		=> __( 'Callout Text', 'pagelines' ),
					),
					array(
						'key'			=> 'k8_poppy_form_disable_callout_text',						
						'label'			=> __( 'Disable callout text', 'pagelines' ),
						'type'			=> 'check',
						'default'		=> false
					),
					array(
						'type' 			=> 'select',
						'key'			=> 'k8_poppy_form_icallout_text_wrap',
						'label' 		=> __( 'Callout Text Wrapper', 'pagelines' ),
						'default'		=> 'h2',
						'opts'			=> array(
							'h1'			=> array('name' => '&lt;h1&gt;'),
							'h2'			=> array('name' => '&lt;h2&gt;  (default)'),
							'h3'			=> array('name' => '&lt;h3&gt;'),
							'h4'			=> array('name' => '&lt;h4&gt;'),
							'h5'			=> array('name' => '&lt;h5&gt;'),
						)
					),

				)
			),
			array(
				'type' 			=> 'multi',
				'title' 		=> 'Button and Email Options',
				'col'			=> 3,
				'opts'	=> array(
										
					array(
						'key'			=> 'k8_poppy_form_icallout_link_text',
						'type' 			=> 'text',
						'label'			=> __( 'Text on Button', 'pagelines' )
					),
					array(
						'key'			=> 'k8_poppy_form_icallout_btn_theme',
						'type' 			=> 'select_button',
						'label'			=> __( 'Button Color', 'pagelines' ),
					),
					
					array(
						'key'			=> 'k8_poppy_form_title',
						'type' 			=> 'text',
						'label'			=> __( 'Form Title', 'pagelines' )
					),					
					array(
						'key'			=> 'k8_poppy_form_to_email',
						'type' 			=> 'text',
						'label'			=> __( 'Email to send to', 'pagelines' )
					),
					array(
						'key'			=> 'k8_poppy_form_enable_captcha',						
						'label'			=> __( 'Enable simple antispam question', 'pagelines' ),
						'type'			=> 'check',
						'default'		=> false
					),
					array(
						'key'			=> 'k8_poppy_form_question',						
						'label'			=> __( 'Antispam question', 'pagelines' ),
						'type'			=> 'text',
						'default'		=> '2+5'
					),
					array(
						'key'			=> 'k8_poppy_form_answer',						
						'label'			=> __( 'Antispam answer', 'pagelines' ),
						'type'			=> 'text',
						'default'		=> '7'
					),
					array(
						'key'			=> 'k8_poppy_form_subject',						
						'label'			=> __( 'Format for email subject. Possible values: %name% %blog%<br />Defaults: [%blog%] New message from %name%.', 'pagelines' ),
						'type'			=> 'text',
						'default'		=> '[%blog%] New message from %name%.'
					)

				)
			)

		);

		return $opts;

	}

	function section_template() {

		$text = $this->opt( 'k8_poppy_form_icallout_text' );
		$disable_text = $this->opt( 'k8_poppy_form_disable_callout_text', array( 'default' => false ) );
		
		$format = ( $this->opt( 'k8_poppy_form_icallout_format' ) ) ? 'format-'.$this->opt( 'k8_poppy_form_icallout_format' ) : 'format-inline';
		$theme = $this->opt( 'k8_poppy_form_icallout_btn_theme', array( 'default' => 'btn-primary' ) );
		$link_text = $this->opt( 'k8_poppy_form_icallout_link_text', array( 'default' => 'Learn More <i class="icon icon-angle-right"></i>' ) );
		$text_wrap = ( '' != $this->opt( 'k8_poppy_form_icallout_text_wrap' ) ) ? $this->opt( 'k8_poppy_form_icallout_text_wrap' ) : 'h2';

		if(!$text && !$link){
			$text = __("Call to action!", 'pagelines');
		}

		$callout_head = sprintf( '<%s class="icallout-head" data-sync="k8_poppy_form_icallout_text">%s</%s>', $text_wrap, $text, $text_wrap );

		?>
		<div class="icallout-container <?php echo $format;?>">
			
			<?php if( ! $disable_text ) echo $callout_head; ?>
			<a class="icallout-action btn <?php echo $theme;?> btn-large" data-toggle="modal" href="#k8-poppy-modal" data-sync="icallout_link_text"><?php echo $link_text; ?></a>

		</div>
	<?php
	
		add_action( 'wp_footer', array( $this, 'form' ) );

	}

	function form() {
		ob_start();
		$email		= __( 'Email Address', 'pagelines' );
		$name		= __( 'Name', 'pagelines' );
		$message	= __( 'Your Message...', 'pagelines' );
		$send		= __( 'Send Message', 'pagelines' );
				
		?>
		<div id="k8-poppy-modal" data-uid="<?php echo $this->meta['unique']; ?>"  class="k8-poppy-modal hide fade modal poppy" >
			<div class="modal-header"><a class="close" data-dismiss="modal" aria-hidden="true">×</a>
				<h3><?php echo $this->opt( 'k8_poppy_form_title' ) ?></h3>
			</div>
			<div class="modal-body">
				<div class="poppy-response"></div>
				<form class="poppy-form" id="k8-ajaxcontactform" action="" method="post" enctype="multipart/form-data">
					<fieldset>
						<div class="control-group">
							<div class="controls form-inline">
								<?php
								printf( '<input class="poppy-input k8-poppy-name" placeholder="%1$s" id="k8-ajaxcontactname" type="text" name="%1$s">', $name );
								printf( '<input class="poppy-input k8-poppy-email" placeholder="%1$s" id="k8-ajaxcontactemail" type="text" name="%1$s">',$email );
								?>								
							</div>
						</div>
					<div class="control-group">
						<div class="controls">
							<div class="textarea">
								<?php printf( '<textarea class="k8-poppy-msg" row="8" placeholder="%s" id="k8-ajaxcontactcontents" name="%s"></textarea>', $message, $message ); ?>
							</div>
						</div>
					</div>
		
					<?php if ( $this->opt( 'k8_poppy_form_enable_captcha' ) ) $this->captcha(); ?>
		
					<div class="controls">
						<?php printf( '<a class="btn btn-link-color k8-send-poppy">%s</a>', $send ); ?>
					</div>
					</fieldset>
				</form>
			</div>
		</div>											
		<?php
		$form = ob_get_clean();
		echo $form;
	}


	function captcha() {

		$code = sprintf( '<div class="control-group">
			<label class="control-label">Captcha</label>
			<div class="controls">
				<input class="span2 k8-poppy-captcha" placeholder="%s" id="ajaxcontactcaptcha" type="text" name="ajaxcontactcaptcha" />
			</div>
		</div>', stripslashes( $this->opt( 'k8_poppy_form_question' ) ) );
		echo $code;
	}
	
	function ajaxcontact_send_mail(){

		$data = stripslashes_deep( $_POST );

		$defaults = array(
			'name'	=> '',
			'email'	=> '',
			'custom'=> '',
			'msg'	=> '',
			'cap'	=> '',
			'width'	=> '',
			'height'=> '',
			'agent' => ''
		);

		$data = wp_parse_args($data, $defaults);

		$name			= $data['name'];
		$email			= $data['email'];
		
		$contents		= $data['msg'];
		$admin_email	= ( $this->opt( 'k8_poppy_form_to_email' ) ) ? $this->opt( 'k8_poppy_form_to_email' ) : get_option( 'admin_email' );
		$captcha		= $data['cap'];
		$captcha_ans	= $this->opt( 'k8_poppy_form_answer' );
		$width			= $data['width'];
		$height			= $data['height'];
		$ip				= $_SERVER['REMOTE_ADDR'];
		$agent			= $data['agent'];

		if ( $this->opt( 'k8_poppy_form_enable_captcha' ) ){
			if( '' == $captcha )
				die( __( 'Captcha cannot be empty!', 'pagelines-poppy' ) );
			if( $captcha !== $captcha_ans )
				die( __( 'Captcha does not match.', 'pagelines-poppy' ) );
		}

		if ( ! filter_var( $email, FILTER_VALIDATE_EMAIL ) ) {
			die( __( 'Email address is not valid.', 'pagelines-poppy' ) );
		} elseif( strlen( $name ) == 0 ) {
			die( __( 'Name cannot be empty.', 'pagelines-poppy' ) );
		} elseif( strlen( $contents ) == 0 ) {
			die( __( 'Content cannot be empty.', 'pagelines-poppy' ) );
		}

		// create an email.
		$subject_template	= ( '' != $this->opt( 'k8_poppy_form_subject' ) ) ? $this->opt( 'k8_poppy_form_subject' ) : '[%blog%] New message from %name%.';
		$subject			= str_replace( '%blog%', get_bloginfo( 'name' ), str_replace( '%name%', $name, $subject_template ) );
		$custom 			= ( $custom_field ) ? sprintf( '%s: %s', $custom_field, $custom ) : '';
		$fields = 'Name: %s %7$sEmail: %s%7$sContents%7$s=======%7$s%s %7$s%7$sUser Info.%7$s=========%7$sIP: %s %7$sScreen Res: %s %7$sAgent: %s %7$s%7$s%8$s';

		$template = sprintf( $fields,
			$name,
			$email,
			$contents,
			$ip,
			sprintf( '%sx%s', $width, $height ),
			$agent,
			"\n",
			$custom
			);
			
		if( true === ( $result = wp_mail( $admin_email, $subject, $template ) ) )
			die( 'ok' );
		
		if ( ! $result ) {

			global $phpmailer;
		
			if( isset( $phpmailer->ErrorInfo ) ) {
				die( sprintf( 'Error: %s', $phpmailer->ErrorInfo ) );
			} else {
				die( __( 'Unknown wp_mail() error.', 'pagelines-poppy' ) );
			}
		}		
	}
}