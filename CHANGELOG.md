## CHANGELOG

= 1.0.0 =
* Initial release 

= 1.0.1 =
* Added option to disable callout text if required

= 1.0.2 =
* K8 Updater compatibility