!function ($) {

	$(document).ready(function() {

		$('.k8-send-poppy').on('click', function(){
			var uid = $(this).parents('.k8-poppy-modal:first').data('uid');
			
			plSendMail( uid )
		})

	})

	function plSendMail( uid ) {

		var name = $('.k8-poppy-modal[data-uid="'+uid+'"] .k8-poppy-name').val()
		,	email = $('.k8-poppy-modal[data-uid="'+uid+'"] .k8-poppy-email').val()
		,	custom = $('.k8-poppy-modal[data-uid="'+uid+'"] .k8-poppy-custom').val()
		,	msg = $('.k8-poppy-modal[data-uid="'+uid+'"] .k8-poppy-msg').val()
		,	captcha = $('.k8-poppy-modal[data-uid="'+uid+'"] .k8-poppy-captcha').val()
		
		console.log($('.k8-poppy-modal[data-uid="'+uid+'"]'));
			
		jQuery.ajax({
			type: 'POST'
			, url: k8_poppyjs.ajaxurl
			, data: {
				action: 'k8_ajaxcontact_send_mail'
				,	name: name
				,	email: email
				,	custom: custom
				,	msg: msg
				,	cap: captcha
				,	width:screen.width
				,	height:screen.height
				,	agent:navigator.userAgent
			}

			,	success: function(response){

					var responseElement = jQuery('.k8-poppy-modal[data-uid="'+uid+'"] .poppy-response')
					var poppyForm = jQuery('.k8-poppy-modal[data-uid="'+uid+'"] .poppy-form')

					responseElement
						.hide()
						.removeClass('alert alert-error alert-success')


					if (response == "ok") {
						responseElement
							.fadeIn()
							.html('Great work! Your message was sent.')
							.addClass('alert alert-success')

						poppyForm
							.html('')

						setTimeout(function() {
							jQuery('.poppy').modal('hide')
						}, 2000)

					} else {
						responseElement
							.fadeIn()
							.html(response)
							.addClass('alert alert-error')
					}
			}

			, error: function(MLHttpRequest, textStatus, errorThrown){
				console.log(errorThrown);
			}

		});

	};

}(window.jQuery);